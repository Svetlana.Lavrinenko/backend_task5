import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

public class Task5 {

    public static void main(String[] args) {

        String text = "Object-oriented programming is a programming language model organized around objects rather than \"actions\" and data rather than logic. Object-oriented programming blabla. Object-oriented programming bla. Ana komok";


        String target = "object-oriented programming";

        String OOP = "OPP";

// first task: Замените в строке каждое второе вхождение «object-oriented programming» на OOP

        int index = 0;

        ArrayList<Integer> listIndex = new ArrayList<>();

        index = text.toLowerCase().indexOf(target);
        while (index >= 0) {
            listIndex.add(index);
            index = text.toLowerCase().indexOf(target, index + 1);
        }

        StringBuilder builder = new StringBuilder();
        int start = 0;
        for (int i = 0; i < listIndex.size(); i++) {

            if (i % 2 == 1) {
                builder.append(text.substring(start, listIndex.get(i))).append("OOP");

                start = listIndex.get(i) + target.length();
            } else {
                builder.append(text, start, listIndex.get(i));
                start = listIndex.get(i);
            }


        }
        builder.append(text, listIndex.get(listIndex.size() - 1), text.length());


        System.out.println(builder.toString());


        //second task Найти слово, в котором число различных символов минимально

        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.addAll(List.of(text.split(" ")));


        int minChar = Integer.MAX_VALUE;
        String result = null;

        for (String word : arrayList) {
            HashSet<Character> set = new HashSet<>();
            for (Character ch : word.toCharArray()) {
                set.add(ch);
            }


            if (minChar > set.size()) {
                minChar = set.size();
                result = word;
            }


        }

        System.out.println(result);


        //third task  Найти количество слов, содержащих только символы латинского алфавита

        int countWords = 0;

        for (String word : arrayList) {
            boolean onlyLetters = true;

            for (Character ch : word.toCharArray()) {
                if (!(ch >= 'A' && ch <= 'Z') && !(ch >= 'a' && ch <= 'z')) {
                    onlyLetters = false;
                    break;
                }

            }

            if (onlyLetters) {
                countWords++;
            }
        }
        System.out.println(countWords);


        //fourth task Найти слова палиндромы

        ArrayList<String> palindrome = new ArrayList<>();



        for (String word : arrayList) {

            StringBuilder builderRev = new StringBuilder(word);

            if (word.toLowerCase().equals(builderRev.reverse().toString().toLowerCase())) {
                palindrome.add(word);

            }

        }

        for(String pal:palindrome)
        {
            System.out.println(pal);
        }

    }
}



