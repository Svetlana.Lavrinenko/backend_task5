Задание №5 11.2022

▸ Введите любую строку. Замените в строке каждое второе вхождение «object-oriented programming» (не учитываем регистр символов) на «OOP»
▸ Например, строка "Object-oriented programming is a programming language model organized around objects rather than "actions" and data rather than logic. Object-oriented programming blabla. Object-oriented programming bla." должна быть преобразована в "Object-oriented programming is a programming language model organized around objects rather than "actions" and data rather than logic. OOP blabla. Object-oriented programming bla."
▸ Найти слово, в котором число различных символов минимально
▸ Слово может содержать буквы и цифры
▸ Если таких слов несколько, найти первое из них
▸ Например, в строке "fffff ab f 1234 jkjk" найденное слово должно быть "fffff"
▸ Найти количество слов, содержащих только символы латинского алфавита
▸ Найти слова палиндромы
